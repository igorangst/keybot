keybot
======

A MIDI-controlled robot playing on a toy keyboard such as a Casio SA-21. See it
in action
[here](https://peertube.metawurst.space/videos/watch/1b57288d-4bf0-4730-86dc-b9a47bf2918c).
